import java.util.Scanner;

public class SoftwareSell {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double discount = .0;
		final double SftPrice = 99.0;
		double FinalPrice = .0;
		double Subtotal = .0;
		
		
		Scanner keyboard = new Scanner(System.in);
		System.out.println("How many software packages do you want? ");
		int SftQty = keyboard.nextInt();
		
		if (SftQty < 10) {
			discount = 1;
		}else if (SftQty >= 10 && SftQty<20) {
			discount = .8;
		}else if (SftQty >= 20 && SftQty<50) {
			discount = .7;
		}else if (SftQty >= 50 && SftQty<100) {
			discount = .6;
		}else if (SftQty >= 100) {
			discount = .5;
		}
		
		Subtotal= SftQty *SftPrice;
		FinalPrice = Subtotal *discount;
		
		System.out.printf("Subtotal: $%.2f\n", Subtotal);
		System.out.printf("Discount: %.0f",(100-discount*100));
		System.out.println("%");
		System.out.printf("Final Price: $%.2f", FinalPrice);
		
		// with printf
		// %d --> integer
		// %f --> double or decimal
	}

}
