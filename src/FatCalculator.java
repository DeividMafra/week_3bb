import java.util.Scanner;

public class FatCalculator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		  	Write a program that calculates if food is healthy or unhealthy.
			The program asks the user for:
			>The total amount of calories in the food
			>The number of grams of fat in the food
			If the percentage of calories from fat is less than 30%, the food is healthy.
			Otherwise, food is unhealthy.
		
			1 gram of fat = 9 calories
			Percentage of calories from fat = 
			(calories from fat) / (total calories)
		 */
						
		Scanner keyboard = new Scanner(System.in);
		double calories;
		double gramsOfFat;
		double fromFat;
			
		System.out.println("What is the total amount of calories in the food?");
		calories = keyboard.nextDouble();
		System.out.println("What is the number of grams of fat in the food?");
		gramsOfFat = keyboard.nextDouble();
		fromFat = gramsOfFat/calories*100;
		if (gramsOfFat>calories) {
			System.out.println("ERROR - Fat is larger than the total of calories");
		}else if (fromFat <30) {
			System.out.printf("Calories from fat = %.2f - Food is healthy", fromFat);
		}else 
			System.out.printf("Calories from fat = %.2f - Food is unhealthy", fromFat);
	}
}
