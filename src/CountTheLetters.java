import java.util.Scanner;

public class CountTheLetters {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
//		Ask the user to enter a string
//		Ask the user to enter a character
//		Calculate the number of times the characters appears in the string

		
		Scanner keyboard = new Scanner(System.in);
		int count=0;
		System.out.println("Enter a String: ");
		String newString = keyboard.nextLine();
		
		System.out.println("Enter a Character to search for: ");
		char newChar = keyboard.next(".").charAt(0);		
		
		char[] charArray = newString.toCharArray();
		
		for (int i = 0; i < charArray.length; i++) {
			if (charArray[i] == newChar) {
				count++;
			}
//			System.out.println(charArray[i]);	
		}
		
//		System.out.println(newString);
//		System.out.println(newChar);
		System.out.printf("The character %c appears %d times in the string %s ", newChar, count, newString);
	}
}
