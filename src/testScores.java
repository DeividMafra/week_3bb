import java.util.Scanner;

public class testScores {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

//		Write a program that asks the user to enter their test scores, and calculates the average.
//		The user can enter as many scores as they want. 
//		(HINT:  Use a loop to repeatedly ask the user for test scores)
//		The user can stop entering scores by typing �-1� into the program.

		Scanner keyboard = new Scanner(System.in);
		double score = 0;
		double sumScore = 0;
		double average = 0;
		int times = 0;
		String value = "";
		try {

			do {
				System.out.println("Enter your test score or -1 to exit: ");
				value = keyboard.nextLine();
				score = Double.parseDouble(value);
				if (score >= -1) {
					if (score >= 0) {
						sumScore += score;
					}
					times++;
				} else {
					System.out.println("You need to enter a number =>0 or -1 to exit!");
				}
			} while (score != -1.0);

			if (times > 1) {
				times--;
				average = sumScore / times;
				System.out.printf("You've entered %d test score(s) and your average is %.2f!", times, average);
			} else
				System.out.println("You didn't enter any score!");

		} catch (Exception e) {
			System.out.println("You need to enter a number =>0 or -1 to exit!");
		}
	}
}