import java.util.Scanner;

public class MakingMoney {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		System.out.println("How many days you will work: ");
		int WorkDays = keyboard.nextInt();
		final double salary= 1;
		double totalDay = 0;
		double total = 0;
		for (int i = 1; i <= WorkDays; i++) {
			totalDay = Math.pow(2, i-1);
			System.out.printf("Day %d = $%.2f\n", i, totalDay);
			total+=totalDay;
		}
		
		System.out.printf("Total = $%.2f", total);

	}

}
