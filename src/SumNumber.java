import java.util.Scanner;

public class SumNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter a positive number: ");
		int PositiveNumber = keyboard.nextInt();
		int total=0;
		
		// with for 
		
//		for ( int i = 1; i <= PositiveNumber; i++) {
//			total +=i;
//		}
//		
//		System.out.println("Sum = " + total);
		
		// OR with while
		int x=0;
		while (x<PositiveNumber) {
			x++;
			total +=x;
		}
		System.out.println("Sum = " + total);
		

	}

}
